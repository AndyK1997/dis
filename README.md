# Distributed Systems ws2020/2021 connected cars
This program simulates a few servers that are networked with each other to analyze randomly generated car sensor data and store it in a database. We generate 4 sensors (fuel level, mileage, traffic situation, average speed) each running in a Docker container, and send the data via MQTT to a broker.  The data is then fetched from the broker by the central and processed further. The control center checks whether the data is valid and then sends it via gRPC to a provider that also runs in Docker. The control center also provides a Json REST api over http. The provider then writes the data to a redundant master sql database and notifies the control center if the write was successful.

german below

Diese Programm simuliert eine paar Server die untereinander vernetzt sind. Um zufällig generierte Auto Sensor Daten auszuwerten und in einer Datenbank abzuspeichern. Wir  generieren 4 Sensoren (Tankfüllstand, Kilometerstand, Verkehrslage, Durchschnittsgeschwindigkeit) die jeweils in einem Docker Container laufen, und die daten über MQTT an einen broker schicken.  Die Daten werden dann von der Zentrale beim Broker abgeholt und weiterverarbeitet. Die Zentrale prüft ob die Daten valide sind und schickt sie dann über gRPC an einen Provider der ebenfalls im Docker läuft. Die Zentrale bietet auch eine Json REST api über http. Der provider schreibt die Daten dann in eine Redundante Master Master Sql Datenbank und teilt der Zentrale mit ob der Schreibvorgang erfolgreich war.
 
## Getting Started :rocket:

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need 16Gb diskspace , 8GB RAM and a CPU with 2 logic cores or better

#### Windows:
1. Installing Ubuntu Subsystem 

```
https://docs.microsoft.com/de-de/windows/wsl/install-win10
```

2. Installing Docker Desktop 

```
https://docs.docker.com/docker-for-windows/install/
```

3. Enable Scripts on Powershell Run Powershell as Admin and execute

```ps1 
Set Set-ExecutionPolicy unrestricted
```

4. Installing git

```
https://git-scm.com/book/de/v2/Erste-Schritte-Git-installieren
```

#### Linux/Debian:
1. Update Packages

```bash
$ sudo apt update && apt upgrade -y
```

2. Installing Docker and docker-compose 

```bash
$ sudo apt install docker && apt install docker-compose -y        
```

### Installing

A step by step series of examples that tell you how to get a development env running

1. Clone the repo

```bash
$ git clone https://code.fbi.h-da.de/distributed-systems/2020_wise_lab/group_c_2.git
```

2. move in Directory

```bash
$ cd group_c_2/docker/
```

3. Configure SQL Database

```bash
$ cd database/server1/
$ sudo chmod -R 0777 log/
$ cd ..
$ cd database/server2/
$ sudo chmod -R 0777 log/
```

#### Fix DB if Master-Master doesn`t work

```bash
$ sudo docker volume rm database_mariadb_1 database_mariadb2_1
$ cd database/server1/data/ && rm -r *
$ cd .. 
$ cd log/ && rm -r *
$ touch log/mysql-bin.index
$ sudo chmod -R 0777 log/
$ cd .. && cd ..
$ cd server2/data/ && rm -r *
$ cd .. 
$ cd log/ && rm -r *
$ touch log/mysql-bin.index
$ sudo chmod -R 0777 log/
$ cd .. && cd ..
$ sudo docker-compose up -d
$ sudo chmod 755 master.sh
$ sudo ./master.sh
$ cd ..
```

### Running and Stop after install

* Run
```bash
$ chmod 755 RunAfterInstall.sh
$ ./RunAfterInstall.sh <yourCountOfCars> 
```

* Stop
```bash
$ chmod 755 Shutdown.sh
$ ./Shutdown.sh <yourCountOfCars>
```

### Running in Debug mode 
You need two Consols

* Consol 1
```bash
$ cd docker/database/
$ docker-compose up 
```
* Consol 2
```bash
$ cd docker/
$ ID="0" port="80" docker-compose -p DEBUG up 
```


## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [CMAKE](https://cmake.org/) - build tool
* [gRPC](https://grpc.io/docs/languages/cpp/quickstart/) - RPC framework
* [mosquitto](https://mosquitto.org/) - MQTT brocker 
* [mariadb](https://mariadb.org/) - Database
* [Docker](https://hub.docker.com/) - vm

## Contributing

Please read for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Andreas Kirchner** -[staskirc](https://gitlab.fbi.h-da.de/istaskirc)
* **Anna-Larissa Berg** -[istaaberg](https://gitlab.fbi.h-da.de/istaaberg)

See also the list of [contributors](https://gitlab.fbi.h-da.de/distributed-systems/2020_wise_lab/group_c_2/-/project_members) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

#### TCP
* https://www.geeksforgeeks.org/tcp-server-client-implementation-in-c/?ref=lbp

#### gRPC
* https://grpc.io/docs/languages/cpp/quickstart/
* https://grpc.io/docs/
* https://github.com/grpc/grpc
* https://developers.google.com/protocol-buffers/docs/cpptutorial

#### MySQL
* https://mariadb.com/kb/en/setting-up-replication/

#### MQTT
* https://mosquitto.org/api/files/mosquitto-h.html#mosquitto_lib_init
* https://mosquitto.org/blog/

#### Docker
* https://linuxhint.com/mysql_docker_compose/
