#include <thread>
#include <mutex>
#include <string>
#include <iostream>
#include <cstdlib>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sstream>
#include <condition_variable>
#include <map>
#include <vector>
#define UDPPORT 1404
#define TCPPORT 8080

using namespace std;

condition_variable cv;
mutex myMutex;
mutex TESTMutex;
int test;
vector<int> clientSockets;

struct Sensor{
    string type;
    string data;
    string timestamp;
    bool persist;
};

multimap<string,Sensor> sensorData;

//Print error Message on the screen
//
void printError(std::string msg){
    std::cerr<<msg<<std::endl;
    exit(0);
}

//Persist @sensorData in the Database
//
void writeInDataBase(){
    while(true){
        unique_lock<mutex> lock(myMutex);
        cv.wait(lock,[]{
            return !sensorData.empty();
        });
        if(!sensorData.empty()){
            for(auto x=sensorData.begin(); x!= sensorData.end(); x++){
                if(x->second.persist==false){
                    //Hier kommt die SQL persistierung rein
                    //
                    //
                    x->second.persist=true;
                }
            }
            cout<<"SQL"<<endl;
            lock.unlock();
            sleep(30);
        }
    }
}

//check DataBase if exist fetch all Sensor Data and load it in @sensorData
//
void readFromDatabase(){

}

//Evaluate the HTML Header
//@return all Params parse out of the Header
vector<string> evaluateHttpHeader(string Header){
    vector<string> httpHeader;
    stringstream header(Header);
    string tmp;
    getline(header,tmp,'/'); tmp.pop_back(); httpHeader.push_back(tmp); //Http method httpHeader[0]
    getline(header,tmp,'/'); httpHeader.push_back(tmp); //HTTP Protokoll typ httpHeader[1]
    getline(header,tmp);     httpHeader.push_back(tmp); //HTTP Version  httpHeader[2]
    getline(header,tmp,':');                            //Host:
    getline(header,tmp);     httpHeader.push_back(tmp); //Hostname  httpHeader[3]
    getline(header,tmp,':');                            //User-Agent:
    getline(header,tmp);     httpHeader.push_back(tmp); //User Agent httpHeader[4]
    getline(header,tmp,':');                            //Accept:
    getline(header,tmp);     httpHeader.push_back(tmp); //accepted protokoll types httpHeader[5]
    getline(header,tmp,':');                            //Acccept-Language:
    getline(header,tmp);     httpHeader.push_back(tmp); //accepted languages httpHeader[6]
    getline(header,tmp,':');                            //Accept-Encoding:
    getline(header,tmp);     httpHeader.push_back(tmp); //accepted encoding types httpHeader[7]
    getline(header,tmp,':');                            //Connection:
    getline(header,tmp);     httpHeader.push_back(tmp); //prefered connection httpHeader[8]

    return httpHeader;
}

//create a valid html Site with contains all Sensor Data
//@return HTML Site as String
string createHtml(){
    const string sensorTypes[4]{"fuel", "mileage", "trafic", "avgspeed"};
    string head="";
    string body="";
    string footer="";
    head="<html><head><meta charset='utf-8'><meta name='keywords' content='car informations, vs praktikum'><meta name='description' content='Sensor Data'><meta name='author' content='Anna &amp; Andreas'><title>Vs-Central</title></head>";
    body="<body><h1>Sensor Daten</h1><table border='1'>";
    scoped_lock<mutex> lock(myMutex);
    for(unsigned int y=0; y<4; y++){
        body+="<tr><td>"+sensorTypes[y]+"</td><td>";
        for(auto x=sensorData.begin();x!=sensorData.end(); x++){
            if(x->first==sensorTypes[y]){
                body+=x->second.data+"|"+x->second.timestamp+"<br>";
            }

        }
        body+="</td></tr>";
    }
    body+="</table></body>";
    footer="</html>";
    return head+body+footer;
}

//Handle running Connections between Client /Server
//@return null if the Thread Terminate
void* tcpConnectionHandler(void* args){
    char buf[4096];
    int newSocket =*((int*)args);
    // While loop: accept and echo message back to client
    while (true) //listen to current host
    {
        memset(buf, 0, 4096);
        // Wait for client to send data
        int bytesReceived = recv(newSocket, buf, 4096, 0);
        if (bytesReceived == -1)
        {
            cerr << "Error in recv(). Quitting" << endl;
            break;
        }
        else if (bytesReceived == 0)
        {
            cout << "Client disconnected Socket id: " <<newSocket<< endl;
            break;
        }
        //Echo Recived Message
        cout << string(buf, 0, bytesReceived) << endl;
        // Echo message back to client
        //http
        string msg=string(buf,0,bytesReceived);
        vector<string> httpHeader=evaluateHttpHeader(msg);
        if(httpHeader.at(0)=="GET"){

            string html=createHtml();
            string httpResponse="HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nServer: central.docker.com /"+httpHeader.at(3)+"\r\nConnection:"+httpHeader.at(8)+"\r\nCache-Control: no-cache\r\nPragma: no-cache\r\nContent-Length: "+to_string(html.size())+"\r\n\r\n"+html;
            sleep(1);
            send(newSocket,httpResponse.c_str(), httpResponse.size(), 0);

            cout<<httpResponse<<endl;
        }else if (httpHeader.at(0)=="HEAD" ||httpHeader.at(0)=="POST" || httpHeader.at(0)=="PUT" || httpHeader.at(0)=="DELETE" ||httpHeader.at(0)=="TRACE"){
            string html="<html><head><title>VS-Central</title></head><body><h1>403 FORBIDDEN</h1></body></html>";
            string httpResponse="HTTP/1.1 403 FORBIDDEN\r\nContent-Type: text/html\r\nServer: central.docker.com /"+httpHeader.at(3)+"\r\nConnection: close\r\nCache-Control: no-cache\r\nPragma: no-cache"+to_string(html.size())+"\r\n\r\n"+html;
            send(newSocket,httpResponse.c_str(), httpResponse.size(),0);
        }else{
            send(newSocket, buf, bytesReceived + 1, 0);
            usleep(1);
        }

    }
    // Close the socket
    close(newSocket);
    return NULL;
}

//Select, Create and save The Sensor in @sensorData
//
void sensorTypeSelector(string &msg){
    string key, value,timestamp;
    Sensor currentSensor;
    const string sensorTypes[4]{"fuel", "mileage", "trafic", "avgspeed"};
    stringstream tmp(msg);
    getline(tmp, key,'|');
    getline(tmp,value,'|');
    getline(tmp,timestamp);
    if(key==sensorTypes[0] || key==sensorTypes[1] || key==sensorTypes[2] || key==sensorTypes[3] ){
        currentSensor.type=key;
        currentSensor.data=value;
        currentSensor.persist=false;
        currentSensor.timestamp=timestamp;
        scoped_lock lock(myMutex);
        cout<<currentSensor.type<<currentSensor.data<<endl;
        sensorData.insert(pair<string,Sensor>(key,currentSensor));
        cv.notify_one();
    }else{
        printError("Unknown Sensor Type");
    }
}

//Thread of TCP Server to handle Tcp Connection
//@return 0 or error Exit Code
int tcpServer(){
    //thread *tcpHandler[5];
    int threadCounter=0;
    // Create a socket
    int listening = socket(AF_INET, SOCK_STREAM, 0);
    if (listening == -1)
    {
        cerr << "Can't create a socket! Quitting" << endl;
        return -1;
    }

    // Bind the ip address and port to a socket
    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(TCPPORT);
    inet_pton(AF_INET, "0.0.0.0", &hint.sin_addr);

    bind(listening, (sockaddr*)&hint, sizeof(hint));

    // Tell Winsock the socket is for listening
    listen(listening, SOMAXCONN);

    while(true){
        // Wait for a connection
        sockaddr_in client;
        socklen_t clientSize = sizeof(client);

        int clientSocket = accept(listening, (sockaddr*)&client, &clientSize);
        char host[NI_MAXHOST];      // Client's remote name
        char service[NI_MAXSERV];   // Service (i.e. port) the client is connect on

        memset(host, 0, NI_MAXHOST); // same as memset(host, 0, NI_MAXHOST);
        memset(service, 0, NI_MAXSERV);

        if (getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
        {
            cout << host << " connected on port " << service<<" Socket id: " <<clientSocket<< endl;
        }
        else
        {
            inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
            cout << host << " connected on port " << ntohs(client.sin_port) <<" Socket id: " <<clientSocket<< endl;
        }

        //close(listening)

        thread tcpHandler(tcpConnectionHandler, &clientSocket);
        tcpHandler.detach();
    }

    return 0;

}

//Thread of UDP Server to recived the Sensor Data
//@return 0 or error Exit Code
int udpServer(){

    int sock, lenght, n;
    socklen_t fromlen;
    struct sockaddr_in server;
    struct sockaddr_in from;
    char buf[1024];
    //string buffer=nullptr;

    sock=socket(AF_INET,SOCK_DGRAM,0);  //udp socket initialisieren
    if(sock<0){
        printError("Cant Open Socket");
    }

    lenght=sizeof(server);
    bzero(&server,lenght);
    server.sin_family=AF_INET;      //IPv4
    server.sin_addr.s_addr=INADDR_ANY; //Accept any ip on the port
    server.sin_port=htons(UDPPORT); //convert to littleendian

    if(bind(sock,(struct sockaddr*)&server,lenght)<0){
        printError("Cant bind Port");
    }

    fromlen=sizeof(struct sockaddr_in);

    while(true){

        n=recvfrom(sock,buf,1023,0,(struct sockaddr*)&from,&fromlen);
        // n=recvfrom(sock,buffer.c_str(),strlen(buffer.c_str()),0,(struct sockaddr*)&from,&fromlen);
        if(n<0){
            printError("Error on Data Transmission");
        }
        string msg=buf;
        sensorTypeSelector(msg);
        cout<<"UDP Recived: "<<msg<<endl;
        //write(1," Received a Datagram: ", 100);
        //write(1,buf,n);
        //n=sendto(sock," Recived your message\n",100,0,(struct sockaddr *)&from,fromlen);

        if(n<0){
            printError("Error code recvfrom: "+to_string(n));
        }
        sleep(1);
    }

    return 0;
}


//Main call TCP, UDP, SQL Threads and join on it
int main(int argc, char* argv[]){

    readFromDatabase();
    thread t1(tcpServer);
    thread t2(udpServer);
    thread t3(writeInDataBase);
    t1.join();
    t2.join();
    t3.join();

    return 0;
}