//
// Created by AndyK on 17.12.2020.
//

#include "Sensor.h"

Sensor::Sensor(std::string type, std::string data, std::string timestamp, bool persist) {
    this->sensorType=type;
    this->sensorData=data;
    this->sensorTimestamp=timestamp;
    this->sensorIsPersist=persist;
}

Sensor  Sensor::getSensor(const std::string &message) {
    const int ARRAY_SIZE=4;
    const std::string sensorTypes[ARRAY_SIZE]{"fuel", "mileage", "trafic", "avgspeed"};
    std::string key, value,timestamp;
    std::stringstream tmp(message);

    getline(tmp, key,'|');
    getline(tmp,value,'|');
    getline(tmp,timestamp);

    if(key==sensorTypes[0] || key==sensorTypes[1] || key==sensorTypes[2] || key==sensorTypes[3] ){

        return Sensor(key, value,timestamp, false);

    }else{

        std::cout<<"cant create sensor invalid Sensor type"<<std::endl;
        exit(EXIT_FAILURE);

    }
}

const std::string &Sensor::getSensorType() const {
    return sensorType;
}

const std::string &Sensor::getSensorData() const {
    return sensorData;
}

const std::string &Sensor::getSensorTimestamp() const {
    return sensorTimestamp;
}

bool Sensor::isSensorIsPersist() const {
    return sensorIsPersist;
}

void Sensor::setSensorIsPersist(bool sensorIsPersist) {
    Sensor::sensorIsPersist = sensorIsPersist;
}
