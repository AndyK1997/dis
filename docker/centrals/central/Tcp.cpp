//
// Created by AndyK on 21.12.2020.
//

#include <arpa/inet.h>
#include <netdb.h>
#include <cstring>
#include <unistd.h>
#include "Tcp.h"

Tcp::Tcp() {
    this->listening=socket(AF_INET, SOCK_STREAM, 0);
    this->hint.sin_family = AF_INET;
    this->hint.sin_port = htons(TCPPORT);
    inet_pton(AF_INET, "0.0.0.0", &hint.sin_addr);
    bind(listening, (sockaddr*)&hint, sizeof(hint));
    // Tell Winsock the socket is for listening
    listen(listening, SOMAXCONN);
}

/**
 * WAIT UNTIL A TCP CONNECTION IS ESTABLISHED
 * @return
 */
int Tcp::tcpConnectionListener() const {

    // Wait for a connection
    sockaddr_in client;
    socklen_t clientSize = sizeof(client);
    int clientSocket = accept(listening, (sockaddr*)&client, &clientSize);
    char host[NI_MAXHOST];      // Client's remote name
    char service[NI_MAXSERV];   // Service (i.e. port) the client is connect on

    memset(host, 0, NI_MAXHOST); // same as memset(host, 0, NI_MAXHOST);
    memset(service, 0, NI_MAXSERV);

    //DNS IP ADDRESS
    if (getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
    {
        std::cout << host << " connected on port " << service<<" Socket id: " <<clientSocket<<std::endl;
    }
    //IP ADDRESS
    else
    {
        inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
        std::cout << host << " connected on port " << ntohs(client.sin_port) <<" Socket id: " <<clientSocket<< std::endl;
    }
    return clientSocket;
}

void Tcp::tcpConnectionHandler(const int &clientSocket, std::mutex &myMutex, std::condition_variable &cv, std::multimap<std::string,Sensor> &sensorData,std::map<std::string,std::string> &connectedSensor, std::mutex &conMutex){
    Http http;
    char buf[4096];
    int newSocket =clientSocket;
    // While loop: accept and echo message back to client
    while (true) //listen to current host
    {
        memset(buf, 0, 4096);
        // Wait for client to send data
        int bytesReceived = recv(newSocket, buf, 4096, 0);

        //INTERRUPT CONNECTION OR ABNORMALLY CLIENT DISCONNECT
        if (bytesReceived == -1)
        {
            std::cerr << "Error in recv(). Quitting" << std::endl;
            break;
        }
        //CLIENT DISCONNECTED NORMALLY
        else if (bytesReceived == 0)
        {
            std::cout << "Client disconnected Socket id: " <<newSocket<< std::endl;
            break;
        }

        //HTTP
        std::string msg=std::string(buf,0,bytesReceived);
        http.parseHttpHeader(msg);
        std::cout<<"HTTP REQUEST: "<<http.getHttpHeader().at(0)<<"|"<<http.getHttpHeader().at(1)<<"|"<<http.getHttpHeader().at(2)<<std::endl;
        //HTTP GET
        if(http.getHttpHeader().at(0)=="GET"){
            std::string html=http.createHtml(sensorData, myMutex, connectedSensor, conMutex);
            std::string httpResponse="HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nServer: central.docker.com /"+http.getHttpHeader().at(2)+"\r\nConnection:"+http.getHttpHeader().at(7)+"\r\nCache-Control: no-cache\r\nPragma: no-cache\r\nContent-Length: "+std::to_string(html.size())+"\r\n\r\n"+html;
            std::this_thread::sleep_for(std::chrono::seconds(1));
            //SEND HTML WEBSITE
            send(newSocket,httpResponse.c_str(), httpResponse.size(), 0);

        //HTTP UNSUPPORTED OPTIONS HEAD POST PUT DELETE TRACE
        }else if (http.getHttpHeader().at(0)=="HEAD" ||http.getHttpHeader().at(0)=="POST" || http.getHttpHeader().at(0)=="PUT" || http.getHttpHeader().at(0)=="DELETE" ||http.getHttpHeader().at(0)=="TRACE"){
            std::string html="<html><head><title>VS-Central</title></head><body><h1>403 FORBIDDEN</h1></body></html>";
            std::string httpResponse="HTTP/1.1 403 FORBIDDEN\r\nContent-Type: text/html\r\nServer: central.docker.com /"+http.getHttpHeader().at(2)+"\r\nConnection: close\r\nCache-Control: no-cache\r\nPragma: no-cache"+std::to_string(html.size())+"\r\n\r\n"+html;
            send(newSocket,httpResponse.c_str(), httpResponse.size(),0);
        //NORMAL TCP CONNECTION
        }else{
            send(newSocket, buf, bytesReceived + 1, 0);
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }
    //CLOSE CURRENT TCP SOCKET
    close(newSocket);
}