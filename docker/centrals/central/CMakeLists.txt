cmake_minimum_required(VERSION 3.16)
project(central)

#set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17 -pthread -pthread -lmosquitto -lmosquittopp")
# Protobuf
set(protobuf_MODULE_COMPATIBLE TRUE)
find_package(Protobuf CONFIG REQUIRED)
message(STATUS "Using protobuf ${protobuf_VERSION}")

# Protobuf-compiler
set(_PROTOBUF_PROTOC $<TARGET_FILE:protobuf::protoc>)

# gRPC
find_package(gRPC CONFIG REQUIRED)
message(STATUS "Using gRPC ${gRPC_VERSION}")
set(_GRPC_GRPCPP gRPC::grpc++)
set(_GRPC_CPP_PLUGIN_EXECUTABLE $<TARGET_FILE:gRPC::grpc_cpp_plugin>)

# Proto file
get_filename_component(hw_proto "rpcTransmission.proto" ABSOLUTE)
get_filename_component(hw_proto_path "${hw_proto}" PATH)

# Generated sources
set(hw_proto_srcs "${CMAKE_CURRENT_BINARY_DIR}/rpcTransmission.pb.cc")
set(hw_proto_hdrs "${CMAKE_CURRENT_BINARY_DIR}/rpcTransmission.pb.h")
set(hw_grpc_srcs "${CMAKE_CURRENT_BINARY_DIR}/rpcTransmission.grpc.pb.cc")
set(hw_grpc_hdrs "${CMAKE_CURRENT_BINARY_DIR}/rpcTransmission.grpc.pb.h")
add_custom_command(
        OUTPUT "${hw_proto_srcs}" "${hw_proto_hdrs}" "${hw_grpc_srcs}" "${hw_grpc_hdrs}"
        COMMAND ${_PROTOBUF_PROTOC}
        ARGS --grpc_out "${CMAKE_CURRENT_BINARY_DIR}"
        --cpp_out "${CMAKE_CURRENT_BINARY_DIR}"
        -I "${hw_proto_path}"
        --plugin=protoc-gen-grpc="${_GRPC_CPP_PLUGIN_EXECUTABLE}"
        "${hw_proto}"
        DEPENDS "${hw_proto}")

# Include generated *.pb.h files

include_directories("${CMAKE_CURRENT_BINARY_DIR}")

find_package(PkgConfig)
pkg_check_modules(LIBMQTT REQUIRED libmosquitto)
pkg_check_modules(LIBMQTTPP REQUIRED libmosquittopp)
link_libraries(${LIBMQTT_LIBRARIES})
link_libraries(${LIBMQTTPP_LIBRARIES})
link_directories(${LIBMQTT_INCLUDE_DIRS})
link_directories(${LIBMQTTPP_INCLUDE_DIRS})


set(SOURCE
        ${SOURCE}
        ${CMAKE_CURRENT_SOURCE_DIR}/Udp.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Tcp.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Http.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Data.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Sensor.cpp
        )
set(HEADERS
        ${HEADERS}
        ${CMAKE_CURRENT_SOURCE_DIR}/Udp.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Tcp.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Http.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Data.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Sensor.h
        Mqtt.cpp Mqtt.h)

# Targets (client|server)
foreach(_target main)
    add_executable(${_target} ${SOURCE} ${HEADERS} "${_target}.cpp"
            ${hw_proto_srcs}
            ${hw_grpc_srcs})
    target_link_libraries(${_target}
            ${_REFLECTION}
            ${_GRPC_GRPCPP}
            ${_PROTOBUF_LIBPROTOBUF})
endforeach()





#add_executable(central main.cpp Sensor.cpp Sensor.h Data.cpp Data.h Http.cpp Http.h Tcp.cpp Tcp.h Udp.cpp Udp.h rpcTransmission.grpc.pb.cc rpcTransmission.grpc.pb.h rpcTransmission.pb.cc rpcTransmission.pb.h)