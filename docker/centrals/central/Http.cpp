//
// Created by AndyK on 17.12.2020.
//

#include "Http.h"


Http::Http() = default;

/**
 * create a valid html Site with contains all Sensor Data
 * @param sensorData
 * @return HTML Site as String
 */
std::string Http::createHtml(std::multimap<std::string, Sensor> &sensorData, std::mutex &mutex, std::map<std::string, std::string> &conectedSensor, std::mutex &conMutex)const {
    std::cout<<"!!!!"<<httpHeader.at(1)<<std::endl;
    std::string head="<html><head><meta charset='utf-8'><meta name='keywords' content='car informations, vs praktikum'><meta name='description' content='Sensor Data'><meta name='author' content='Anna Berg &amp; Andreas Kirchner'><title>Vs-Central</title></head>";
    mutex.lock();
    std::string body=createHtmlBody(sensorData, conectedSensor,conMutex);
    mutex.unlock();
    body+="</body>";
    std::string footer="</html>";

    return head+body+footer;
}

/**
 * HTTP GET URL localhost/fuel, localhost/trafic, localhost/mileage, localhost/avgspeed, localhost/list
 * @param Header of HTTP Request
 * @return all Params parse out of the Header
 */
std::string Http::createHtmlBody(std::multimap<std::string,Sensor> &sensorData, std::map<std::string, std::string> &conectedSensor, std::mutex &conMutex)const  {
    std::string body;

    if(httpHeader.at(0)=="GET"){
       // body="<body><h1>Sensor Daten</h1><table border='1'>";
        if(httpHeader.at(1).find(sensorTypes[0])!=std::string::npos){
           //body+= createHtmlTable(sensorData,fuel);
            body+= createJson(sensorData,fuel);
        }else if(httpHeader.at(1).find(sensorTypes[1])!=std::string::npos){
            //body+= createHtmlTable(sensorData,mileage);
            body+= createJson(sensorData,mileage);
        }else if(httpHeader.at(1).find(sensorTypes[2])!=std::string::npos){
            //body+= createHtmlTable(sensorData,trafic);
            body+= createJson(sensorData,trafic);
        }else if(httpHeader.at(1).find(sensorTypes[3])!=std::string::npos){
            //body+= createHtmlTable(sensorData,avgspeed);
            body+= createJson(sensorData,avgspeed);
        }else if(httpHeader.at(1).find("list")!=std::string::npos){
             body+= createSensorStatusHTMLTable(conectedSensor,conMutex);
        }else{
            //body+= createHtmlTable(sensorData,all);
            body+= createJson(sensorData,all);
        }

    }else if (httpHeader.at(0)=="HEAD" ||httpHeader.at(0)=="POST" || httpHeader.at(0)=="PUT" || httpHeader.at(0)=="DELETE" ||httpHeader.at(0)=="TRACE"){
         body="<html><head><title>VS-Central</title></head><body><h1>403 FORBIDDEN</h1></body></html>";
        }
    return body;
}

/**
 * EVALUATE HTTP HEADER
 * @param Header
 */
void Http::parseHttpHeader(std::string Header) {
    this->httpHeader.clear();
    std::stringstream header(Header);
    std::string tmp;
    getline(header,tmp,'/'); tmp.pop_back(); httpHeader.push_back(tmp); //Http method httpHeader[0]
    getline(header,tmp); httpHeader.push_back(tmp); //HTTP Protokoll + /zusatz
    getline(header,tmp,':');                            //Host:
    getline(header,tmp);     httpHeader.push_back(tmp); //Hostname  httpHeader[2]
    getline(header,tmp,':');                            //User-Agent:
    getline(header,tmp);     httpHeader.push_back(tmp); //User Agent httpHeader[3]
    getline(header,tmp,':');                            //Accept:
    getline(header,tmp);     httpHeader.push_back(tmp); //accepted protokoll types httpHeader[4]
    getline(header,tmp,':');                            //Acccept-Language:
    getline(header,tmp);     httpHeader.push_back(tmp); //accepted languages httpHeader[5]
    getline(header,tmp,':');                            //Accept-Encoding:
    getline(header,tmp);     httpHeader.push_back(tmp); //accepted encoding types httpHeader[6]
    getline(header,tmp,':');                            //Connection:
    getline(header,tmp);     httpHeader.push_back(tmp); //prefered connection httpHeader[7]
}

/**
 * CREATE A TABLE HTML OF SENSOR VALUES
 * @param sensorData
 * @param type
 * @return
 */
std::string Http::createHtmlTable(std::multimap<std::string, Sensor> &sensorData, int type) {
    std::string htmlTable;
    std::string tmpSensorType;
    bool allTypes=false;
    switch(type){
        case fuel :  tmpSensorType=sensorTypes[fuel];
        break;
        case mileage : tmpSensorType=sensorTypes[mileage];
        break;
        case trafic : tmpSensorType=sensorTypes[trafic];
        break;
        case avgspeed : tmpSensorType=sensorTypes[avgspeed];
        break;
        default: allTypes=true;
    }
    if(!allTypes){
        htmlTable="<tr><td>"+tmpSensorType+"</td><td>";
        for(auto x=sensorData.begin();x!=sensorData.end(); x++){
            if(x->first==tmpSensorType){
                htmlTable+=x->second.getSensorData()+"|"+x->second.getSensorTimestamp()+"<br>";
            }
        }
        htmlTable+="</td></tr>";
    }else{
        for(unsigned int y=0; y<4; y++){
            htmlTable+="<tr><td>"+sensorTypes[y]+"</td><td>";
            for(auto x=sensorData.begin();x!=sensorData.end(); x++){
                if(x->first==sensorTypes[y]){
                    htmlTable+=x->second.getSensorData()+"|"+x->second.getSensorTimestamp()+"<br>";
                }
            }
            htmlTable+="</td></tr>";
        }
    }
        return htmlTable;
}

/**
 * CREATE A JSON STRING AS REST API
 * @param sensorData
 * @param type
 * @return
 */
std::string Http::createJson(std::multimap<std::string, Sensor> &sensorData, int type)const  {
    std::string json;
    std::string tmpSensorType;
    bool allTypes=false;
    switch(type){
        case fuel :  tmpSensorType=sensorTypes[fuel];
            break;
        case mileage : tmpSensorType=sensorTypes[mileage];
            break;
        case trafic : tmpSensorType=sensorTypes[trafic];
            break;
        case avgspeed : tmpSensorType=sensorTypes[avgspeed];
            break;
        default: allTypes=true;
    }
    //CONTAINS ONE SELECTED SENSOR
    if(!allTypes){
        json="{\""+tmpSensorType+"\":[";
        for(auto x=sensorData.begin();x!=sensorData.end(); x++){
            if(x->first==tmpSensorType){
                json+="{\"data\":\""+x->second.getSensorData()+"\",\"timestamp\":"+"\""+x->second.getSensorTimestamp()+"\"},";
            }
        }
        json.pop_back(); //DELETE ,
        json+="]";
    //CONTAINS ALL SENSOR
    }else{
        json="{";
        for(unsigned int y=0; y<4; y++){

            json+="\""+sensorTypes[y]+"\":[";
            for(auto x=sensorData.begin();x!=sensorData.end(); x++){
                if(x->first==sensorTypes[y]){
                    json+="{\"data\":\""+x->second.getSensorData()+"\",\"timestamp\":"+"\""+x->second.getSensorTimestamp()+"\"},";
                }

            }
            json.pop_back(); //DELETE ,

             json+="],";
        }
        json.pop_back();
    }
    return json+="}";
}


const std::vector<std::string> &Http::getHttpHeader() const  {
    return httpHeader;
}

/**
 * CREATE A TABLE OF ALL CONNECTED SENSORS AND SHOWS THE CONNECTION STATE
 * @param conectedSensor
 * @param conMutex
 * @return
 */
std::string Http::createSensorStatusHTMLTable(std::map<std::string, std::string> &conectedSensor, std::mutex &conMutex) const {
    std::string htmlTable="<table border='1'><tr><th>Sensors</th><th>Status</th>";
    std::scoped_lock<std::mutex> lock(conMutex);
    for(auto x=conectedSensor.begin(); x!=conectedSensor.end(); x++){
        htmlTable+="<tr>";
        if(checkSensorConnection(x->second)){
            htmlTable+="<td>"+x->first+"</td><td>OK</td></tr>";
        }else{
            htmlTable+="<td>"+x->first+"</td><td>ERROR</td></tr>";
        }
    }
    htmlTable+="</table>";
    return htmlTable;
}

/**
 * CHECK SENSOR IS ALIVE
 * @param time
 * @return
 */
bool Http::checkSensorConnection(std::string time) const {
    std::time_t t;
    std::time(&t);
    std::tm* now=std::localtime(&t);
    std::cout<<"Test Sensor Connection";
    int hour =atoi(time.substr(time.find(':')+1,2).c_str());
    int min =atoi(time.substr(time.find(':')+4,2).c_str());
    std::cout<<time<<" compare "<<hour<<":"<<min<<std::endl;
    if((hour==now->tm_hour || hour+1==now->tm_hour) && (min==now->tm_min || min+1==now->tm_min || min+2==now->tm_min)){
        return true;
    }
    return false;
}


