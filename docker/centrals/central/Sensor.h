//
// Created by AndyK on 17.12.2020.
//

#ifndef CENTRAL_SENSOR_H
#define CENTRAL_SENSOR_H
#include <string>
#include <sstream>
#include <iostream>
#include <condition_variable>
class Sensor {
private:
    std::string sensorType;
    std::string sensorData;
    std::string sensorTimestamp;
    bool sensorIsPersist;
public:
    Sensor(std::string type,std::string data, std::string timestamp,bool persist);
    static Sensor getSensor(const std::string &message) ;
    const std::string &getSensorType() const;

    const std::string &getSensorData() const;

    const std::string &getSensorTimestamp() const;

    bool isSensorIsPersist() const;

    void setSensorIsPersist(bool sensorIsPersist);

};


#endif //CENTRAL_SENSOR_H
