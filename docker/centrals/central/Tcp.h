//
// Created by AndyK on 21.12.2020.
//

#ifndef CENTRAL_TCP_H
#define CENTRAL_TCP_H


#include <netinet/in.h>
#include <iostream>
#include "Http.h"
#include <mutex>
#include <condition_variable>
#include <thread>
#define TCPPORT 8080

class Tcp {
private:
    int listening;
    sockaddr_in hint;
public:
    Tcp();
    int tcpConnectionListener() const ;
    static void tcpConnectionHandler(const int &clientSocket, std::mutex &myMutex, std::condition_variable &cv, std::multimap<std::string,Sensor> &sensorData, std::map<std::string,std::string> &connectedSensor, std::mutex &conMutex);
};


#endif //CENTRAL_TCP_H
