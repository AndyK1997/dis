//
// Created by AndyK on 17.12.2020.
//

#ifndef CENTRAL_HTTP_H
#define CENTRAL_HTTP_H


#include <string>
#include <vector>
#include <sstream>
#include <map>
#include "Sensor.h"
#include <memory>
#include <mutex>
//#include <time.h>
#include <sys/time.h>
class Http {
private:
    const std::string sensorTypes[4]{"fuel", "mileage", "trafic", "avgspeed"};
    enum sensorCase{fuel=0, mileage, trafic, avgspeed, all};
    std::vector<std::string> httpHeader;
public:
    const std::vector<std::string> &getHttpHeader() const;

public:
    Http();
    std::string createHtmlBody(std::multimap<std::string,Sensor> &sensorData,std::map<std::string, std::string> &conectedSensor, std::mutex &conMutex)const ;
    void parseHttpHeader(std::string Header);
    std::string createHtml(std::multimap<std::string,Sensor> &sensorData, std::mutex &mutex, std::map<std::string, std::string> &conectedSensor, std::mutex &conMutex)const ;
    std::string createHtmlTable(std::multimap<std::string,Sensor> &sensorData, int type); // PRINT SENSOR DATA IN HTML
    std::string createJson(std::multimap<std::string, Sensor> &sensorData, int type)const;
    std::string createSensorStatusHTMLTable(std::map<std::string, std::string> &conectedSensor, std::mutex &conMutex)const;
    bool checkSensorConnection(std::string time)const;
};


#endif //CENTRAL_HTTP_H
