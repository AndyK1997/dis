//
// Created by AndyK on 21.12.2020.
//

#ifndef CENTRAL_UDP_H
#define CENTRAL_UDP_H
#include <sys/socket.h>
#include <netinet/in.h>
#include <mutex>
#include <condition_variable>
#include "Sensor.h"
#include <map>
#define UDPPORT 1404

class Udp {
private:
    int sock, lenght, n;
    socklen_t fromlen;
    struct sockaddr_in server;
    struct sockaddr_in from;
public:
    Udp();
    void registrSocket();
    void recvUdpData(std::mutex &myMutex, std::condition_variable &cv, std::multimap<std::string,Sensor> &sensorData);


};


#endif //CENTRAL_UDP_H
