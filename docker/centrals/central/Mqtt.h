//
// Created by AndyK on 30.12.2020.
//

#ifndef CENTRAL_MQTT_H
#define CENTRAL_MQTT_H
#include <string>
#include <mosquitto.h>
#include <iostream>
#include "Sensor.h"
class Mqtt {
private:
    int rc;
    struct mosquitto *mosq;
    std::string brokerIp;
    int brokerPort;
    std::string mosqID;
public:
    Mqtt(std::string mosqID, std::string brokerIp, int brokerPort );
    ~Mqtt();
    static void on_connect(struct mosquitto *mosq, void *obj, int rc);
    static void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg);
    int setBrokerSubscribe();
    int getRc() const;
    const std::string &getBrokerIp() const;

    mosquitto *getMosq() const;
};



#endif //CENTRAL_MQTT_H
