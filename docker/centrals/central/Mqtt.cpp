//
// Created by AndyK on 30.12.2020.
//

#include "Mqtt.h"
Mqtt::Mqtt(std::string mosqID, std::string brokerIp, int brokerPort ) {
    mosquitto_lib_init();
    this->mosqID=mosqID;
    this->mosq=mosquitto_new(mosqID.c_str(),true,NULL);
    this->brokerIp=brokerIp;
    this->brokerPort=brokerPort;
}

Mqtt::~Mqtt() {

    mosquitto_destroy(this->mosq);
    mosquitto_lib_cleanup();
}

void Mqtt::on_connect(struct mosquitto *mosq, void *obj, int rc) {
    std::cout<<"client ID:"<<*(int *)obj<<std::endl;
    if(rc!=0){
        std::cerr<<"Error with result code:"<<rc<<std::endl;
        exit(EXIT_FAILURE);
    }
    mosquitto_subscribe(mosq,NULL,"Sensor",0);
}

void Mqtt::on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg){
    std::cout<<"New message recived, topic:"<<msg->topic<<" Payload: "<<(char*)msg->payload<<std::endl;
}

int Mqtt::getRc() const {
    return rc;
}

mosquitto *Mqtt::getMosq() const {
    return mosq;
}

const std::string &Mqtt::getBrokerIp() const {
    return brokerIp;
}
