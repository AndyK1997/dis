//
// Created by AndyK on 17.12.2020.
//

#ifndef CENTRAL_DATA_H
#define CENTRAL_DATA_H
#include<map>
#include "Sensor.h"
#include <mutex>
#include <condition_variable>
#include <thread>
#include <grpcpp/grpcpp.h>
#include "rpcTransmission.pb.h"
#include "rpcTransmission.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using rpcTransmission::Request;
using rpcTransmission::Response;
using rpcTransmission::Transmission;
using rpcTransmission::load;
using rpcTransmission::Empty;
class Data {
private:
    std::unique_ptr<Transmission::Stub> stub_; //INTERFACE PROVIDER 1
    std::unique_ptr<Transmission::Stub> stub2_; //INTERFACE PROVIDER 2
    std::unique_ptr<Transmission::Stub> stub3_; //INTERFACE PROVIDER 3

    std::multimap<std::string, Sensor> sensorData;
    int centralId;
public:
    Data(std::shared_ptr<Channel> channel, std::shared_ptr<Channel> channel2, std::shared_ptr<Channel> channel3, int centrId) : stub_(Transmission::NewStub(channel)), stub2_(Transmission::NewStub(channel2)),stub3_(Transmission::NewStub(channel3)), centralId(centrId) {}
    bool writeInDataBase(std::multimap<std::string,Sensor> &sensorData);
    void readFromDatabase(std::multimap<std::string,Sensor> &sensorData);
    bool loadData(std::multimap<std::string,Sensor> &sensorData);
    int sendRequest(const std::string& sensorType, const std::string& sensorValue, const std::string& sensorTimestamp);
    std::multimap<std::string, Sensor>& getSensorData(std::mutex &myMutex);
    int balancingRpc(int rpcServerId,Request &request, Response &response);
};


#endif //CENTRAL_DATA_H
