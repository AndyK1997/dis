#include <thread>
#include <mutex>
#include <string>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <condition_variable>
#include <map>
#include <vector>
#include "Sensor.h"
#include "Data.h"
#include "Tcp.h"
#include "Udp.h"
#include "Mqtt.h"

using namespace std;

multimap<string, Sensor> sensorData;
map<string, string> connectedSensor;
bool connected = false;
mutex myMutex;
mutex conMutex;
condition_variable cv;
std::string MQTT_TOPIC;


void TEST(Data &data) {
    multimap<string, Sensor> testData;
    Sensor invalidSensor("test", "foo", "bar", false);
    testData.insert(pair<string, Sensor>("test", invalidSensor));
    data.writeInDataBase(testData);
    testData.clear();
    for (int x = 0; x < 100; x++) {
        testData.insert(pair<string, Sensor>("fuel", Sensor("fuel", "50", "2021.1.28:15:44:00", false)));
    }
    data.writeInDataBase(testData);
    cout << "Test complete" << endl;
}

/**
 * RPC THREAD SEND sensorData VIA gRPC -> provider
 * @param dataMutex
 * @param cv
 * @param sensorData
 * @param data
 */
void rpcSendData(mutex &dataMutex, condition_variable &cv, multimap<string, Sensor> &sensorData, Data &data){
    while (true) {
        unique_lock<mutex> lock(dataMutex);
        cv.wait(lock);
        if (data.writeInDataBase(sensorData)) {
            lock.unlock();
            this_thread::sleep_for(chrono::seconds(15));
        } else {
            lock.unlock();
        }

    }
}


/**
 * TCP THREAD LISTEN AND HANDLE TCP CONNECTIONS
 * @param dataMutex
 * @param cv
 * @param sensorData
 */
void tcpThread(mutex &dataMutex, condition_variable &cv, multimap<string, Sensor> &sensorData,
               map<string, string> &connectedSensor, mutex &conMutex) {
    Tcp tcp;
    while (true) {
        int newConnection = tcp.tcpConnectionListener();
        thread tcpHandler(Tcp::tcpConnectionHandler, ref(newConnection), ref(dataMutex), ref(cv), ref(sensorData),
                          ref(connectedSensor), ref(conMutex));
        tcpHandler.detach();
    }
}


/**
 * Thread of UDP Server to recived the Sensor Data
 * @param myMutex
 * @param cv
 * @param sensorData
 */
void udpThread(mutex &myMutex, condition_variable &cv, multimap<string, Sensor> &sensorData) {
    Udp udp;
    udp.registrSocket();
    while (true) {
        udp.recvUdpData(myMutex, cv, sensorData);
        this_thread::sleep_for(chrono::seconds(1));
    }
}

/**
 * CONNECT MQTT TO BROKER
 * @param mosq
 * @param obj
 * @param rc
 */
void on_connect(struct mosquitto *mosq, void *obj, int rc) {

    //CONNECTION FAILURE
    if (rc != 0) {
        std::cerr << "Error with result code:" << rc << std::endl;
        exit(EXIT_FAILURE);
    }
    //CONNECTION SUCCESS
    mosquitto_subscribe(mosq, NULL, MQTT_TOPIC.c_str(), 0);
}

/**
 * RECEIVE SUBSCRIBE MESSAGE
 * @param mosq
 * @param obj
 * @param msg
 */
void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg) {
    std::cout << "Mqtt Message Received, topic:" << msg->topic << " Payload: " << reinterpret_cast<char *>(msg->payload)
              << std::endl;

    connected = false;
    std::string message = reinterpret_cast<char *>(msg->payload);
    Sensor sensor = Sensor::getSensor(message);
    std::scoped_lock<std::mutex> lock(myMutex);
    sensorData.insert(std::pair<std::string, Sensor>(sensor.getSensorType(), sensor));
    std::scoped_lock<std::mutex> lock2(conMutex);

    for (auto x = connectedSensor.begin(); x != connectedSensor.end(); x++) {
        if (x->first == sensor.getSensorType()) {
            x->second = sensor.getSensorTimestamp();
            connected = true;
            break;
        }
    }
    if (connected == false) {
        connectedSensor.insert(
                std::pair<std::string, std::string>(sensor.getSensorType(), sensor.getSensorTimestamp()));
    }
    cv.notify_one(); //NOTIFY RPC THREAD
}

/**
 * MQTT THREAD HANDLE AND SUBSCRIBE
 * @param mqttIp
 * @param myMutex
 * @param cv
 * @param sensorData
 * @param centralId
 */
void mqttThread(const char *mqttIp, mutex &myMutex, condition_variable &cv, multimap<string, Sensor> &sensorData,
                int &centralId) {
    int rc, id = 12;
    mosquitto_lib_init();
    std::string mosqId = "central" + std::to_string(centralId);

    while (true) {
        struct mosquitto *mosq;
        mosq = mosquitto_new(mosqId.c_str(), true, &id);
        mosquitto_connect_callback_set(mosq, on_connect);
        mosquitto_message_callback_set(mosq, on_message);
        rc = mosquitto_connect(mosq, mqttIp, 1883, 10);

        //CONNECTION FAILURE
        if (rc != 0) {
            std::cerr << "Couldt not connect to Broker with return code: " << rc << std::endl;

        //CONNECTION SUCCESS
        } else {
            mosquitto_loop_forever(mosq, 10, 5);
            mosquitto_loop_stop(mosq, true);
            mosquitto_disconnect(mosq);
            mosquitto_destroy(mosq);
            mosquitto_lib_cleanup();
        }
    }
}



/**
 * MAIN CALL TCP MQTT gRPC THREADS
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[]) {

    std::string grpcIp = argv[1],grpcPort = argv[2], grpcIp2 = argv[4],grpcIp3 = argv[5];
    const char *mqttBrokerIp = argv[3];
    int centralId = atoi(argv[6]);
    MQTT_TOPIC = argv[7];

    //gRPC IPs AND PORT
    std::string address(grpcIp + ":" + grpcPort),address2(grpcIp2 + ":" + grpcPort), address3(grpcIp3 + ":" + grpcPort);

    Data data(grpc::CreateChannel(address, grpc::InsecureChannelCredentials()),
              grpc::CreateChannel(address2, grpc::InsecureChannelCredentials()),
              grpc::CreateChannel(address3, grpc::InsecureChannelCredentials()), centralId);

    std::cout << "Start up Central please wait ..." << std::endl;

    //CHECK IF TEST MODE IS SET
    if (argc > 7 && atoi(argv[8]) == 1) {
        TEST(data);
    //NORMAL MODE
    } else {
        //LOAD DATA ON START UP
        data.readFromDatabase(sensorData);

        thread t1(tcpThread, ref(myMutex), ref(cv), ref(sensorData), ref(connectedSensor), ref(conMutex));
        //thread t2(udpThread, ref(myMutex), ref(cv), ref(sensorData)); //UDP
        thread t3(rpcSendData, ref(myMutex), ref(cv), ref(sensorData), ref(data));
        thread t4(mqttThread, ref(mqttBrokerIp), ref(myMutex), ref(cv), ref(sensorData), ref(centralId));
        t1.join();
        //t2.join(); //UDP
        t3.join();
        t4.join();
    }
    return 0;
}
