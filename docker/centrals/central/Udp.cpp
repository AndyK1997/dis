//
// Created by AndyK on 21.12.2020.
//

#include <strings.h>
#include "Udp.h"

Udp::Udp() {

}

void Udp::registrSocket() {
    sock=socket(AF_INET,SOCK_DGRAM,0);  //udp socket initialisieren
    if(sock<0){
        std::cerr<<("Cant Open Socket")<<std::endl;
    }

    lenght=sizeof(server);
    bzero(&server,lenght);
    server.sin_family=AF_INET;      //IPv4
    server.sin_addr.s_addr=INADDR_ANY; //Accept any ip on the port
    server.sin_port=htons(UDPPORT); //convert to littleendian

    if(bind(sock,(struct sockaddr*)&server,lenght)<0){
        std::cerr<<("Cant bind Port")<<std::endl;
    }

    fromlen=sizeof(struct sockaddr_in);
}

void Udp::recvUdpData(std::mutex &myMutex, std::condition_variable &cv,std::multimap<std::string, Sensor> &sensorData){

    char buf[1024];

        this->n = recvfrom(sock, buf, 1023, 0, (struct sockaddr *) &from, &fromlen);
        if (this->n < 0) {
            std::cerr<<("Error on Data Transmission")<<std::endl;
        }
        std::string msg = buf;
        Sensor sensor = Sensor::getSensor(msg);
        std::cout << "UDP Recived: " << msg << std::endl;
        std::scoped_lock<std::mutex> lock(myMutex);
        sensorData.insert(std::pair<std::string, Sensor>(sensor.getSensorType(), sensor));
        cv.notify_one();
        if (this->n < 0) {
            std::cerr << ("Error code recvfrom: " + std::to_string(n));
        }
}
