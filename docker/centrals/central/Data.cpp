//
// Created by AndyK on 17.12.2020.
//

#include <unistd.h>
#include "Data.h"


/**
 *check DataBase if exist fetch all Sensor Data and load it in @sensorData
 */
void Data::readFromDatabase(std::multimap<std::string, Sensor> &sensorData) {

    if (!loadData(sensorData)) {
        std::cerr << "Cant connect to Database try it again in 10 sec" << std::endl;
        sleep(10);
        loadData(sensorData);
    }
}

/**
 * CALL gRPC SEND REQUEST FOR ALL UNPERSIST SENSORS AND SHOW THE TRANSMISSION STATE
 */
bool Data::writeInDataBase(std::multimap<std::string, Sensor> &sensorData) {
    if (!sensorData.empty()) {
        for (auto &x : sensorData) {
            if (!x.second.isSensorIsPersist()) {

                int response;
                response = sendRequest(x.second.getSensorType(), x.second.getSensorData(),
                                       x.second.getSensorTimestamp());
                std::cout << "RPC code: " << response << std::endl;
                x.second.setSensorIsPersist(true);
            }
        }
        return true;
    }
    return false;
}


std::multimap<std::string, Sensor> &Data::getSensorData(std::mutex &myMutex) {
    std::scoped_lock<std::mutex> lock(myMutex);
    return this->sensorData;
}

/**
 * CREATE A gRPC REQUEST
 * @param sensorType
 * @param sensorValue
 * @param sensorTimestamp
 * @return
 */
int Data::sendRequest(const std::string& sensorType, const std::string& sensorValue, const std::string& sensorTimestamp) {
    Request request;
    request.set_sensortyp(sensorType);
    request.set_sensorvalue(sensorValue);
    request.set_sensortimestamp(sensorTimestamp);
    request.set_centralid(this->centralId);
    Response response;
    return balancingRpc(rand() % 3, request, response); //CALL LOAD BALANCER
}


/**
 * LOAD ALL PERSIST DATA VIA gRPC FROM PROVIDER
 * @param sensorData
 * @return
 */
bool Data::loadData(std::multimap<std::string, Sensor> &sensorData) {
    bool success = true;
    load l;
    Empty empty;
    empty.set_centralid(this->centralId);
    ClientContext context;
    //LOAD FROM PROVIDER 1
    Status status = stub_->loadData(&context, empty, &l);
    if (status.ok()) {
        for (unsigned int x = 0; x < l.sensordata_size(); x++) {
            Request res = l.sensordata().at(x);
            Sensor sensor(res.sensortyp(), res.sensorvalue(), res.sensortimestamp(), true);
            sensorData.insert(std::pair<std::string, Sensor>(sensor.getSensorType(), sensor));
        }
    //LOAD FROM PROVIDER 2
    } else {
        std::cerr << "cant load data from Sql1" << std::endl;
        ClientContext context2;
        Status status2 = stub2_->loadData(&context2, empty, &l);
        if (status2.ok()) {
            for (unsigned int x = 0; x < l.sensordata_size(); x++) {
                Request res = l.sensordata().at(x);
                Sensor sensor(res.sensortyp(), res.sensorvalue(), res.sensortimestamp(), true);
                sensorData.insert(std::pair<std::string, Sensor>(sensor.getSensorType(), sensor));
            }
        //LOAD FROM PROVIDER 3
        } else {
            std::cerr << "cant load data from Sql1 and Sql2" << std::endl;
            ClientContext context3;
            Status status3 = stub3_->loadData(&context3, empty, &l);
            if (status3.ok()) {
                for (unsigned int x = 0; x < l.sensordata_size(); x++) {
                    Request res = l.sensordata().at(x);
                    Sensor sensor(res.sensortyp(), res.sensorvalue(), res.sensortimestamp(), true);
                    sensorData.insert(std::pair<std::string, Sensor>(sensor.getSensorType(), sensor));
                }
            //ALL PROVIDERS ARE NOT AVAILABLE
            } else {
                success = false;
            }
        }
    }

    return success;
}

/**
 * PROVIDER LOAD BALANCER
 * @param rpcServerId
 * @param request
 * @param response
 * @return
 */
int Data::balancingRpc(const int rpcServerId, Request &request, Response &response){
    Status status;
    ClientContext context;
    enum rpcServer {
        PROVIDER1, PROVIDER2, PROVIDER3
    };
    switch (rpcServerId) {
        case PROVIDER1 : {
            status = stub_->sendRequest(&context, request, &response);
            if (!status.ok()) {
                std::cout << "Provider 1 offline code: " << status.error_code() << ": " << status.error_message()
                          << std::endl;
                balancingRpc(1, request, response); //RECURSIVE CALL
            }
            break;
        }
        case PROVIDER2 : {
            status = stub2_->sendRequest(&context, request, &response);
            if (!status.ok()) {
                std::cout << "Provider 2 offline code: " << status.error_code() << ": " << status.error_message()
                          << std::endl;
                balancingRpc(2, request, response); //RECURSIVE CALL
            }
            break;
        }
        case PROVIDER3: {
            status = stub3_->sendRequest(&context, request, &response);
            if (!status.ok()) {
                std::cout << "Provider 3 offline code: " << status.error_code() << ": " << status.error_message()
                          << std::endl;
                status = stub_->sendRequest(&context, request, &response);
                if (!status.ok()) {
                    std::cout << "Provider 1 offline code: " << status.error_code() << ": " << status.error_message()
                              << std::endl;
                    status = stub2_->sendRequest(&context, request, &response);
                    if (!status.ok()) {
                        std::cout << "Provider 2 offline code: " << status.error_code() << ": "
                                  << status.error_message() << std::endl;
                        return -1;
                    }
                }
            }
            break;
        }
        default: {
            return -1;
        }
    }
    return response.transmissionstate();
}