#!/bin/bash
docker stop database_provider_1
echo provider1 offline!
sleep 60
docker start database_provider_1
echo provider1 online!
docker stop database_mariadb_1
echo  mariadb1 offline!
sleep 60
docker start database_mariadb_1
echo mariadb1 online!
sleep 20
docker stop database_mariadb2_1
echo mariadb2 offline!
sleep 60
docker start database_mariadb2_1
echo mariadb2 online!
docker stop database_provider_1 
docker stop database_mariadb_1
echo mariadb1 and provider1 offline!
sleep 60
docker start database_provider_1
docker start database_mariadb_1

docker stop database_provider_1
docker stop database_provider2_1
echo provider1 and provider2 offline!
sleep 60
docker stop database_mariadb_1
echo  mariadb1 and provider1 and provider2 offline!
sleep 60
docker start database_mariadb_1
docker start database_provider_1
docker start database_provider2_1
echo all online! exit test


