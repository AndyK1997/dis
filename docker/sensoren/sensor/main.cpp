
#ifdef _WIN32
#include <Windows.h>
#else

#include <unistd.h>

#endif

#include <iostream>
#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include "Sensor.h"
#include "Transmission.h"
#include "Mqtt.h"
#include <chrono>
#include <thread>

#define ARRAY_SIZE 4
#define PARAM_MIN_SIZE 1
#define PARAM_MAX_SIZE 7

void TEST(std::string ip, std::string mode, std::string port){
    Mqtt *mqtt;
    Sensor *sensor;
    Sensor *stmp;
    sensor = Sensor::createSensor("fuel", ip, port);
    sensor->createRandValues(0);
    if (sensor != nullptr) {
        std::cout << "Test 1 Passed Sensor Successfully created" << std::endl;

    }
    stmp = Sensor::createSensor("fuel", ip, port);
    if (stmp == nullptr) {
        std::cout << "Test 2 Passed only one Sensor per Container" << std::endl;
    }
    mqtt = new Mqtt(sensor->getSensorTyp(), ip.c_str(), 1883, "test");
    sensor->getTimestamp();
    std::cout << "Start Test 3 at" << sensor->getSensorValueTimestamp() << std::endl;
    for (unsigned int x = 0; x < 100; x++) {
        sensor->launchSensor(0);
        sensor->getTimestamp();
        int checkTransmission = mqtt->sendMessageToBroker(sensor);
        if (checkTransmission == 0) {
            continue;
        } else {
            delete mqtt;
            mqtt = new Mqtt(sensor->getSensorTyp(), ip.c_str(), 1883, "test");
            sleep(1);
        }
    }
    sensor->getTimestamp();
    std::cout
            << "Test 3 Passed  send 100 messages to broker " + sensor->getSensorValueTimestamp() + " (fire and forget)"
            << std::endl;
    exit(EXIT_SUCCESS);
}


int main(int argc, char *argv[]) {
    const std::string allowedValues[ARRAY_SIZE]{"fuel", "mileage", "trafic", "avgspeed"};
    Sensor *sensor;
    //Transmission *transmission; //udp
    Mqtt *mqtt;
    int sensorTypeIndex = -1;
    if (argc > PARAM_MIN_SIZE && argc < PARAM_MAX_SIZE) {
        std::string mode = argv[1];
        std::string ip = argv[2];
        std::string port = argv[3];
        std::string topic = argv[4];

        //CHECK TEST MODE IS ENABLED
        if (argc >= 6 && atoi(argv[5]) == 1) {
            TEST(ip, mode, port);
        }

        std::cout << mode << ip << port << std::endl;

        //CHECK IF TYPE IS VALID
        for (int x = 0; x < ARRAY_SIZE; x++) {
            if (allowedValues[x] == mode) {
                sensor = Sensor::createSensor(mode, ip, port);
                sensor->createRandValues(x);
                sensorTypeIndex = x;
                break;
            }
        }

        //transmission = new Transmission(sensor); //udp
        mqtt = new Mqtt(sensor->getSensorTyp(), ip.c_str(), 1883, topic);

        while (true) {
            sensor->launchSensor(sensorTypeIndex);
            sensor->getTimestamp();
            //int checkTransmission = transmission->sendDataVia(sensor); //udp
            int checkTransmission = mqtt->sendMessageToBroker(sensor);
            if (checkTransmission == 0) {
                sleep(30 + rand() % 5);

            } else {
                //delete transmission;  //udp
                //transmission = nullptr;  //udp
                //transmission = new Transmission(sensor);  //udp
                delete mqtt;
                mqtt = new Mqtt(sensor->getSensorTyp(), ip.c_str(), 1883, topic);
                sleep(1);
            }
        }

    } else {

//programm will terminate if not valid Sensor mode was given 
        std::cout << "invalid args error" << std::endl;
    }
    return 0;
}