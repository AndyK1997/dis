//
// Created by AndyK on 17.12.2020.
//

#ifndef SENSOR_SENSOR_H
#define SENSOR_SENSOR_H

#include <string>
#include <cstdlib>
#include <iostream>


class Sensor {
private :
    std::string sensorTyp;
public:
    const std::string &getSensorTyp() const;

    const std::string &getSensorIp() const;

    const std::string &getSensorPort() const;

    const std::string &getSensorValue() const;

    const std::string &getSensorValueTimestamp() const;

    static Sensor *createSensor(std::string sensorTyp, std::string sensorIp, std::string sensorPort);

    void launchSensor(const int sensorType);

    void fuelSensor();

    void mileageSensor();

    void traficSensor();

    void avgSpeedSensor();

    void createRandValues(const int sensorType);

    void getTimestamp();

private:
    std::string sensorIp;
    std::string sensorPort;
    std::string sensorValue;
    std::string sensorValueTimestamp;

    Sensor(std::string sensorTyp, std::string sensorIp, std::string sensorPort);


};


#endif //SENSOR_SENSOR_H
