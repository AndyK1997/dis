//
// Created by AndyK on 17.12.2020.
//

#include "Transmission.h"

Transmission::Transmission(const Sensor * sensor) {
    this->h = gethostbyname(sensor->getSensorIp().c_str()); //holt sich ip Daten von OS
    this->remoteServAddr.sin_family = this->h->h_addrtype; //
    memcpy((char *) &remoteServAddr.sin_addr.s_addr, h->h_addr_list[0], h->h_length);
    this->remoteServAddr.sin_port = htons(atoi(sensor->getSensorPort().c_str()));
    this->socketFileDiscriptor = socket(AF_INET, SOCK_DGRAM,
                                        0);  //SOCK_DGRAM ist UDP SOCK_STREAM ist TCP ; Initialisieren Socket bei OS ; 0 stand in Man page
    this->cliAddr.sin_family = AF_INET;  //ipv4 Protocol
    this->cliAddr.sin_addr.s_addr = htonl(INADDR_ANY);  //konvertiert ip zu little endian 32bit
    this->cliAddr.sin_port = htons(0);  //little endian 16bit
    this->remoteConnection = bind(socketFileDiscriptor, (struct sockaddr *) &cliAddr,
                                  sizeof(cliAddr)); //bindet port an localhost
}

bool Transmission::checkTransmissionReq(const Sensor * sensor)const {

    /* IP-Adresse vom Server überprüfen */
    if (h == NULL) {
        std::cout << "unbekannter Host" << sensor->getSensorIp() << std::endl;
        exit(EXIT_FAILURE);
    }
    /* Socket erzeugen */
    if (socketFileDiscriptor < 0) {
        std::cout << "Kann Socket nicht öffnen" << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }
    /* Jeden Port bind(en) */
    if (remoteConnection < 0) {
        std::cout << "Konnte Port nicht binden" << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }
    return true;

}

int Transmission::sendDataVia(const Sensor * sensor) {

    if (checkTransmissionReq(sensor)) {
        std::cout << "sende Daten an " << h->h_name << " IP: " << inet_ntoa(*(struct in_addr *) h->h_addr_list[0]);

        /* Daten senden */
        std::string transmissionValue;
        transmissionValue =
                sensor->getSensorTyp() + "|" + sensor->getSensorValue() + "|" + sensor->getSensorValueTimestamp();
        remoteConnection = sendto(socketFileDiscriptor, transmissionValue.c_str(),
                                  strlen(transmissionValue.c_str()) + 1, 0, (struct sockaddr *) &remoteServAddr,
                                  sizeof(remoteServAddr)); //länge von ip adresse

        if (remoteConnection < 0) {
            std::cout << "Konnte Daten nicht senden" << std::endl;
            close(socketFileDiscriptor);
            exit(EXIT_FAILURE);
        }
    }else{
     return -1;
    }
    return 0;
}