//
// Created by AndyK on 30.12.2020.
//

#ifndef SENSOR_MQTT_H
#define SENSOR_MQTT_H

#include <string>
#include <mosquitto.h>
#include <iostream>
#include "Sensor.h"

class Mqtt {
private:
    int rc;
    struct mosquitto *mosq;
    std::string brokerIp;
    int brokerPort;
    std::string topic;
public:
    Mqtt(std::string mosqID, std::string brokerIp, int brokerPort, std::string topic);

    ~Mqtt();

    int sendMessageToBroker(const Sensor *sensor);
};


#endif //SENSOR_MQTT_H
