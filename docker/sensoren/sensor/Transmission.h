//
// Created by AndyK on 17.12.2020.
//

#ifndef SENSOR_TRANSMISSION_H
#define SENSOR_TRANSMISSION_H

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <errno.h>
#include <cstring>
#include "Sensor.h"

//UDP CLASS
class Transmission {
private:
    int socketFileDiscriptor, remoteConnection, i; //socket file wird nummeriert
    struct sockaddr_in cliAddr, remoteServAddr;
    struct hostent *h; //hostname in host struct  char *h_name;  char **h_aliases; int h_addrtype; int h_length; char **h_addr_list;
public:
    Transmission(const Sensor * sensor);

    int sendDataVia(const Sensor * sensor);

    bool checkTransmissionReq(const Sensor * sensor)const ;
};


#endif //SENSOR_TRANSMISSION_H
