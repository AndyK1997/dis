//
// Created by AndyK on 30.12.2020.
//

#include "Mqtt.h"

Mqtt::Mqtt(std::string mosqID, std::string brokerIp, int brokerPort, std::string topic) {
    this->topic = topic;
    mosquitto_lib_init();
    this->mosq = mosquitto_new(mosqID.c_str(), true, NULL);
    this->brokerIp = brokerIp;
    this->brokerPort = brokerPort;
}

Mqtt::~Mqtt() {

    mosquitto_destroy(this->mosq);
    mosquitto_lib_cleanup();
}

/**
 * SEND MQTT MESSAGES TO BROKER
 * @param sensor
 * @return
 */
int Mqtt::sendMessageToBroker(const Sensor *sensor){

    //PAYLOAD
    std::string msg = sensor->getSensorTyp() + "|" + sensor->getSensorValue() + "|" + sensor->getSensorValueTimestamp();

    //TRANSMISSION STATE
    this->rc = mosquitto_connect(this->mosq, this->brokerIp.c_str(), this->brokerPort, 60);

    //CONNECTION FAILURE
    if (this->rc != 0) {
        std::cerr << "Client couldt connect to brocker! Error Code:" << this->rc << std::endl;
        mosquitto_destroy(this->mosq);
        return -1;
    //CONNECTION SUCCESS
    } else {
        std::cout << "connected send Data: " << msg << std::endl;
        mosquitto_publish(this->mosq, NULL, topic.c_str(), msg.size(), msg.c_str(), 0, false);
        mosquitto_disconnect(this->mosq);
    }
    return 0;
}