//
// Created by AndyK on 17.12.2020.
//

#include <unistd.h>
#include "Sensor.h"

static bool exist = false;

Sensor::Sensor(std::string sensorTyp, std::string sensorIp, std::string sensorPort) {
    this->sensorTyp = sensorTyp;
    this->sensorIp = sensorIp;
    this->sensorPort = sensorPort;

}

/**
 * Sensor Factory Create only one Sensor
 * @param sensorTyp
 * @param sensorIp
 * @param sensorPort
 * @return
 */
Sensor *Sensor::createSensor(std::string sensorTyp, std::string sensorIp, std::string sensorPort) {
    Sensor *singelton;
    if (exist == false) {
        singelton = new Sensor(sensorTyp, sensorIp, sensorPort);
        exist = true;
    } else {
        singelton = nullptr;
        std::cerr << "You can create only one Sensor" << std::endl;
    }
    return singelton;
}

/**
 *SIMULATE FUEL CONSUMPTION
 */
void Sensor::fuelSensor() {

    //REFUEL
    if (atoi(this->sensorValue.c_str()) <= 0) {
        this->sensorValue = std::to_string(100);
        sleep(30);
        //NORMAL FUEL CONSUMPTION
    } else {
        int subtrac = rand() % 5;
        if ((atoi(this->sensorValue.c_str()) - subtrac) >= 0) {
            this->sensorValue = std::to_string(atoi(this->sensorValue.c_str()) - subtrac);
        } else {
            this->sensorValue = std::to_string(0);
        }

    }
}

/**
 * INCREASE MILEAGE
 */
void Sensor::mileageSensor() {
    this->sensorValue = std::to_string(atoi(this->sensorValue.c_str()) + 1);
}

/**
 * CHOOSE RAND TRAFFIC SITUATION
 */
void Sensor::traficSensor() {
    const int ARRAY_SIZE = 4;
    const int ENUMVAR = 2;
    createRandValues(ENUMVAR);
    const std::string cases[ARRAY_SIZE]{"frei", "maessig", "stark", "stau"};
    std::string current = cases[atoi(this->sensorValue.c_str())]; //INDEX OF SITUATION
    this->sensorValue = current; //INDEX TO CASE[INDEX]

}

/**
 * CREATE RAND AVERAGE SPEED
 */
void Sensor::avgSpeedSensor() {

    const int ENUMVAR = 3;
    int avgspeed = 0, speed;

    for (unsigned int x = 0; x < 10; x++) {
        speed = atoi(this->sensorValue.c_str());
        avgspeed += speed;
        sleep(1);
        createRandValues(ENUMVAR);
    }

    avgspeed = avgspeed / 10; //CALC AVERAGE
    this->sensorValue = std::to_string(avgspeed);
}

/**
 * SENSOR MODE SELECTOR
 * @param sensorType
 */
void Sensor::launchSensor(const int sensorType){
    enum sensorTypes {
        fuel = 0, mileage, trafic, avgspeed
    };
    switch (sensorType) {
        case fuel:
            fuelSensor();
            break;
        case mileage :
            mileageSensor();
            break;
        case trafic :
            traficSensor();
            break;
        case avgspeed :
            avgSpeedSensor();
            break;
        default:
            std::cerr << "invalid sesnor value" << std::endl;
    }
}


/**
 * CREATE RAND VALUES FOR EACH SENSOR
 * @param sensorType
 */
void Sensor::createRandValues(const int sensorType) {
    enum sensorTypes {
        fuel = 0, mileage, trafic, avgspeed
    };

    srand(time(NULL));

    switch (sensorType) {
        case fuel: {
            int value = rand() % 50;
            value += 50;
            this->sensorValue = std::to_string(value);
            break;
        }
        case mileage :
            this->sensorValue = std::to_string(rand() % 200000);
            break;
        case trafic :
            sleep(15);
            this->sensorValue = std::to_string(rand() % 4);
            break;
        case avgspeed :
            this->sensorValue = std::to_string(rand() % 250);
            break;
        default:
            std::cerr << "Invalid Sensor Type in rand func" << std::endl;
    }
}

/**
 * CREATE TIMESTAMP
 */
void Sensor::getTimestamp() {
    time_t timestamp = time(0);
    tm *currentTime = localtime(&timestamp);
    std::string year = std::to_string(currentTime->tm_year + 1900);
    std::string month = std::to_string(currentTime->tm_mon + 1);
    std::string day = std::to_string(currentTime->tm_mday);
    std::string hour = std::to_string(currentTime->tm_hour);
    std::string min = std::to_string(currentTime->tm_min);
    std::string sec = std::to_string(currentTime->tm_sec);
    this->sensorValueTimestamp = year + "." + month + "." + day + ":" + hour + ":" + min + ":" + sec;
}

const std::string &Sensor::getSensorTyp() const {
    return sensorTyp;
}

const std::string &Sensor::getSensorIp() const {
    return sensorIp;
}

const std::string &Sensor::getSensorPort() const {
    return sensorPort;
}

const std::string &Sensor::getSensorValue() const {
    return sensorValue;
}

const std::string &Sensor::getSensorValueTimestamp() const {
    return sensorValueTimestamp;
}

