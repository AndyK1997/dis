#!/bin/bash
docker-compose -f database/docker-compose.yml up -d
whileCounter=0
port=80;
while [ $whileCounter -lt $1 ] 
do
port=$(( $port + $whileCounter ))
ID="${whileCounter}" port="${port}" docker-compose -p vs$whileCounter up -d
whileCounter=$(( $whileCounter + 1 ))
done
