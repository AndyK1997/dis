//
// Created by AndyK on 28.12.2020.
//

#ifndef CENTRAL_SQL_H
#define CENTRAL_SQL_H
#include <mysql/mysql.h>
#include <string>
#include <iostream>
struct connection_details{
    const char *server, *user, *password, *database;
};

class Sql {
private:
  //char* mysql_details;
  connection_details mySqlDetails;
    MYSQL *con;
    MYSQL_RES *res;

public:
    MYSQL* mysql_connection_setup();
    MYSQL_RES* mysql_execute_query(MYSQL *connection, const char *sql_query)const;
    Sql(connection_details mysql_details);
    void query(std::string sqlQuery);
    MYSQL *getCon() const;
    MYSQL_RES *getRes() const;

};


#endif //CENTRAL_SQL_H
