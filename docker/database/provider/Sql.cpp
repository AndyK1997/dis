//
// Created by AndyK on 28.12.2020.
//
#include "Sql.h"


Sql::Sql(const connection_details mysql_details) {
    this->mySqlDetails=mysql_details;
}

MYSQL* Sql::mysql_connection_setup(){
     con=mysql_init(nullptr);
    if(!mysql_real_connect(con, this->mySqlDetails.server,this->mySqlDetails.user, this->mySqlDetails.password,this->mySqlDetails.database,0,NULL,0)){
        std::cerr<<"Connection Error: "<<mysql_error(con)<<std::endl;
        throw -1;
    }

    return con;
}

MYSQL_RES* Sql::mysql_execute_query(MYSQL *connection, const char *sql_query)const{
    if(mysql_query(connection,sql_query)){
        std::cerr<<"MySQL Query Error: "<<mysql_error(connection)<< std::endl;
        throw -2;
    }
    return mysql_use_result(connection);
}

void Sql::query(const std::string sqlQuery) {
    this->con = mysql_connection_setup();
    this->res = mysql_execute_query(this->con, sqlQuery.c_str());
}

MYSQL_RES *Sql::getRes() const {
    return res;
}

MYSQL *Sql::getCon() const {
    return con;
}


