//
// Created by AndyK on 23.12.2020.
//

#include <grpcpp/grpcpp.h>
#include "rpcTransmission.pb.h"
#include "rpcTransmission.grpc.pb.h"
#include "Sql.h"
#include <exception>

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

using rpcTransmission::Request;
using rpcTransmission::Response;
using rpcTransmission::Transmission;


class TestServerImplemetation final : public Transmission::Service {
private:
    Sql *sql1;
    Sql *sql2;
    std::string sqlIp1;
    std::string sqlIp2;
    const int ARRAY_SIZE = 4;
    const std::string validSensorTypes[4]{"fuel", "mileage", "trafic", "avgspeed"};
public:
    TestServerImplemetation(const std::string sqlIp, const std::string sqlIp2, const std::string sqlUser, const std::string sqlpw) {
        this->sqlIp1 = sqlIp;
        this->sqlIp2 = sqlIp2;
        connection_details mysqlDatabase1{};
        mysqlDatabase1.server = sqlIp.c_str();
        mysqlDatabase1.user = "root";
        mysqlDatabase1.password = "password";
        mysqlDatabase1.database = "vs_praktikum";
        this->sql1 = new Sql(mysqlDatabase1);

        connection_details mysqlDatabase2{};
        mysqlDatabase2.server = sqlIp2.c_str();
        mysqlDatabase2.user = "root";
        mysqlDatabase2.password = "password";
        mysqlDatabase2.database = "vs_praktikum";
        this->sql2 = new Sql(mysqlDatabase2);

    }

    Status sendRequest(::grpc::ServerContext *context, const ::rpcTransmission::Request *request,
                       ::rpcTransmission::Response *response) override {
        bool dataValid = false;
       const std::string& sensorType = request->sensortyp();
       const std::string& sensorValue = request->sensorvalue();
       const std::string& sensorTimeStamp = request->sensortimestamp();
       const std::string& centralId=std::to_string(request->centralid());

        for (unsigned int x = 0; x < ARRAY_SIZE; x++) {
            if (sensorType == this->validSensorTypes[x]) {
                dataValid = true;
                break;
            }
        }
        if (dataValid) {
            response->set_transmissionstate(0);
            std::cout << "rpcRecived:" << sensorType << "|" << sensorValue << "|" << sensorTimeStamp << "|" << centralId << std::endl;
            const std::string queryString =
                    "INSERT INTO `Sensor` (`sensorType`, `sensorValue`, `sensorTimestamp`, `centralId`) VALUES ('" + sensorType + "','" + sensorValue + "','" + sensorTimeStamp + "','" + centralId + "')";
            while (true) {
                try {
                    sql1->query(queryString);
                    mysql_close(sql1->getCon());
                    break;
                } catch (int Exception) {
                    if(Exception==-1){
                        try {
                            sql2->query(queryString);
                            mysql_close(sql2->getCon());
                            break;
                        } catch (int e) {
                            continue;
                        }
                    }
                }
            }

        } else {
            response->set_transmissionstate(-1);

        }
        return Status::OK;

    }

    Status sendResponse(::grpc::ServerContext *context, const ::rpcTransmission::Empty *request,
                        ::rpcTransmission::Response *response) override {
        return Service::sendResponse(context, request, response);
    }

    Status loadData(::grpc::ServerContext *context, const ::rpcTransmission::Empty *request,
                    ::rpcTransmission::load *response) override {
        const std::string queryString="SELECT * FROM Sensor Where centralId='"+std::to_string(request->centralid())+"'";
        try {
            sql1->query(queryString);
            MYSQL_ROW row;
            for (int x = 0; (row = mysql_fetch_row(sql1->getRes())) != NULL; x++) {
                auto pair = response->mutable_sensordata();
                Request Sensor;
                Sensor.set_sensortyp(row[1]);
                Sensor.set_sensorvalue(row[2]);
                Sensor.set_sensortimestamp(row[3]);
                (*pair)[x] = Sensor;
            }
            mysql_free_result(sql1->getRes());
            mysql_close(sql1->getCon());
            return Status::OK;
        } catch (int e) {
            if(e==-1){
                try {
                    MYSQL_ROW row;
                    sql2->query(queryString);
                    for (int x = 0; (row = mysql_fetch_row(sql2->getRes())) != NULL; x++) {
                        auto pair = response->mutable_sensordata();
                        Request Sensor;
                        Sensor.set_sensortyp(row[1]);
                        Sensor.set_sensorvalue(row[2]);
                        Sensor.set_sensortimestamp(row[3]);
                        (*pair)[x] = Sensor;
                    }
                    mysql_free_result(sql2->getRes());
                    mysql_close(sql2->getCon());
                    return Status::OK;
                } catch (int e) {
                    //do nothing
                }
            }
        }
        return Status::CANCELLED;
    }


};

int main(int argc, char *argv[]) {
    const std::string sqlUser = "root", sqlpw = "password";
    std::string sqlServer1Ip, sqlServer2Ip, rpcReplyIp, rpcPort;
    if (argc >= 4) {
        rpcReplyIp = argv[1];
        rpcPort = argv[2];
        sqlServer1Ip = argv[3];
        sqlServer2Ip = argv[4];
    } else {
        rpcReplyIp = "127.0.0.1";
        rpcPort = "50000";
        sqlServer1Ip = "127.0.0.1";
        sqlServer2Ip = "127.0.0.1";
    }

    TestServerImplemetation service(sqlServer1Ip, sqlServer2Ip, sqlUser, sqlpw);

    const std::string address(rpcReplyIp + ":" + rpcPort);
    std::cout << address << std::endl;


    ServerBuilder builder;

    builder.AddListeningPort(address, grpc::InsecureServerCredentials());
    builder.RegisterService(&service);

    std::unique_ptr<Server> server(builder.BuildAndStart());
    std::cout << "Server listening on port: " << address << std::endl;
    server->Wait();

    return 0;
}

