function cut {
  param(
    [Parameter(ValueFromPipeline=$True)] [string]$inputobject,
    [string]$delimiter='\s+',
    [string[]]$field
  )

    process {
    if ($field -eq $null) { $inputobject -split $delimiter } else {
      ($inputobject -split $delimiter)[$field] }
  }
}


docker exec -ti database_mariadb_1 sh -c "mysql -uroot -ppassword --execute 'source /backup/initdb.sql'"
docker exec -ti database_mariadb2_1 sh -c "mysql -uroot -ppassword --execute 'source /backup/initdb.sql'"
Start-Sleep -s 10
$binServer1 = docker exec -ti database_mariadb_1 sh -c "mysql -uroot -ppassword --execute ' use vs_praktikum; show master status\g;'"| select -last 2 |select -first 1 |cut -d "\|" -f 1
$positionServer1= docker exec -ti database_mariadb_1 sh -c "mysql -uroot -ppassword --execute ' use vs_praktikum; show master status\g;'"| select -last 2 |select -first 1 |cut -d "\|" -f 2 
$binServer1=$binServer1.trim();
$positionServer1=$positionServer1.trim();

$binServer2 = docker exec -ti database_mariadb2_1 sh -c "mysql -uroot -ppassword --execute ' use vs_praktikum; show master status\g;'"| select -last 2 |select -first 1 |cut -d "\|" -f 1
$positionServer2= docker exec -ti database_mariadb2_1 sh -c "mysql -uroot -ppassword --execute ' use vs_praktikum; show master status\g;'"| select -last 2 |select -first 1 |cut -d "\|" -f 2 
$binServer2=$binServer2.trim();
$positionServer2=$positionServer2.trim();
echo $binServer1 $positionServer1
#echo "use vs_praktikum; stop slave; CHANGE MASTER TO MASTER_HOST = 'mysql_test_mariadb2_1', MASTER_USER = 'public', MASTER_PASSWORD = 'mysql1pass', MASTER_LOG_FILE = '$binServer1', MASTER_LOG_POS =$positionServer1; start slave;"
docker exec -ti database_mariadb_1 sh -c "mysql -uroot -ppassword  --execute ' use vs_praktikum; stop slave;'"
docker exec -ti database_mariadb_1 sh -c "mysql -uroot -ppassword  --execute 'CHANGE MASTER TO MASTER_HOST = '\''database_mariadb2_1'\'', MASTER_USER = '\''public'\'', MASTER_PASSWORD = '\''mysql1pass'\'', MASTER_LOG_FILE = '\''$binServer1'\'', MASTER_LOG_POS =$positionServer1;'"
docker exec -ti database_mariadb_1 sh -c "mysql -uroot -ppassword  --execute ' use vs_praktikum; start slave;'"

docker exec -ti database_mariadb2_1 sh -c "mysql -uroot -ppassword  --execute ' use vs_praktikum; stop slave;'"
docker exec -ti database_mariadb2_1 sh -c "mysql -uroot -ppassword  --execute 'CHANGE MASTER TO MASTER_HOST = '\''database_mariadb_1'\'', MASTER_USER = '\''public'\'', MASTER_PASSWORD = '\''mysql1pass'\'', MASTER_LOG_FILE = '\''$binServer2'\'', MASTER_LOG_POS =$positionServer2;'"
docker exec -ti database_mariadb2_1 sh -c "mysql -uroot -ppassword  --execute ' use vs_praktikum; start slave;'"
#docker exec -ti mysql_test_mariadb2_1 sh -c "mysql -uroot -ppassword --execute 'use vs_praktikum; stop slave; CHANGE MASTER TO MASTER_HOST = \'mysql_test_mariadb_1\', MASTER_USER = \'public\', MASTER_PASSWORD = \'mysql1pass\', MASTER_LOG_FILE = \'"+$binServer2+"\', MASTER_LOG_POS =$positionServer2; start slave;'"

