CREATE DATABASE IF NOT EXISTS vs_praktikum
DEFAULT CHARACTER SET utf8
COLLATE utf8_unicode_ci;
USE vs_praktikum;
grant replication slave on *.* to 'public'@'%';
# do note that the replicator permission cannot be granted on single database.
FLUSH PRIVILEGES;
SHOW MASTER STATUS;
SHOW VARIABLES LIKE 'server_id';


-- ----------------------------------------------------------------

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";
-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `article`
--

CREATE TABLE IF NOT EXISTS `Sensor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sensorType` varchar(10) NOT NULL,
  `sensorValue` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sensorTimestamp` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `centralId` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten f�r Tabelle `article`
--

INSERT INTO `Sensor` (`sensorType`, `sensorValue`, `sensorTimestamp`, `centralid`) VALUES
('fuel','50','2020-12-28:10:10:10','1'),
('fuel','50','2020-12-28:10:10:10','1'),
('fuel','50','2020-12-28:10:10:10','1');