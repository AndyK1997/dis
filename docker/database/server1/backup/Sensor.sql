-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 13. Apr 2020 um 14:05
-- Server-Version: 10.4.11-MariaDB

-- PHP-Version: 7.4.2

-- /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
-- /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
-- /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
-- /*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `vs_praktikum`
--

CREATE DATABASE IF NOT EXISTS vs_praktikum
DEFAULT CHARACTER SET utf8
COLLATE utf8_unicode_ci;

USE vs_praktikum;

-- ----------------------------------------------------------------

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";
-- --------------------------------------------------------

--
-- Tabellenstruktur f�r Tabelle `article`
--

CREATE TABLE `Sensor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sensorType` varchar(10) NOT NULL,
  `sensorValue` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sensorTimestamp` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten f�r Tabelle `article`
--

INSERT INTO `Sensor` (`sensorType`, `sensorValue`, `sensorTimestamp`) VALUES
('fuel','50','2020-12-28:10:10:10'),
('fuel','50','2020-12-28:10:10:10'),
('fuel','50','2020-12-28:10:10:10');

