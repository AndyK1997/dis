#!/bin/bash
#docker volume rm $(docker volume ls -q)
#cd server1/data/ &&rm -r *
#cd .. 
#cd ..
#cd server2/data/ &&rm -r *
docker exec -ti database_mariadb_1 sh -c "mysql -uroot -ppassword --execute 'source /backup/initdb.sql'"
docker exec -ti database_mariadb2_1 sh -c "mysql -uroot -ppassword --execute 'source /backup/initdb.sql'"
binServer1=$(docker exec -ti database_mariadb_1 sh -c "mysql -uroot -ppassword --execute ' use vs_praktikum; show master status\g;'"| tail -n 2 |head -n 1 |cut -d '|' -f 2 | xargs)
positionServer1=$(docker exec -ti database_mariadb_1 sh -c "mysql -uroot -ppassword --execute ' use vs_praktikum; show master status\g;'"| tail -n 2 |head -n 1 |cut -d '|' -f 3  | xargs)
echo $binServer1 $positionServer1
binServer2=$(docker exec -ti database_mariadb2_1 sh -c "mysql -uroot -ppassword --execute ' use vs_praktikum; show master status\g;'"| tail -n 2 |head -n 1 |cut -d '|' -f 2 | xargs)
positionServer2=$(docker exec -ti database_mariadb2_1 sh -c "mysql -uroot -ppassword --execute ' use vs_praktikum; show master status\g;'"|tail -n 2 |head -n 1 |cut -d '|' -f 3  | xargs)
echo $binServer2 $positionServer2
#echo "use vs_praktikum; stop slave; CHANGE MASTER TO MASTER_HOST = 'mysql_test_mariadb2_1', MASTER_USER = 'public', MASTER_PASSWORD = 'mysql1pass', MASTER_LOG_FILE = '$binServer1', MASTER_LOG_POS =$positionServer1; start slave;"
docker exec -ti database_mariadb_1 sh -c "mysql -uroot -ppassword  --execute ' use vs_praktikum; stop slave;'"
docker exec -ti database_mariadb_1 sh -c "mysql -uroot -ppassword  --execute 'CHANGE MASTER TO MASTER_HOST = '\''database_mariadb2_1'\'', MASTER_USER = '\''public'\'', MASTER_PASSWORD = '\''mysql1pass'\'', MASTER_LOG_FILE = '\''$binServer1'\'', MASTER_LOG_POS =$positionServer1;'"
docker exec -ti database_mariadb_1 sh -c "mysql -uroot -ppassword  --execute ' use vs_praktikum; start slave;'"

docker exec -ti database_mariadb2_1 sh -c "mysql -uroot -ppassword  --execute ' use vs_praktikum; stop slave;'"
docker exec -ti database_mariadb2_1 sh -c "mysql -uroot -ppassword  --execute 'CHANGE MASTER TO MASTER_HOST = '\''database_mariadb_1'\'', MASTER_USER = '\''public'\'', MASTER_PASSWORD = '\''mysql1pass'\'', MASTER_LOG_FILE = '\''$binServer2'\'', MASTER_LOG_POS =$positionServer2;'"
docker exec -ti database_mariadb2_1 sh -c "mysql -uroot -ppassword  --execute ' use vs_praktikum; start slave;'"
#docker exec -ti mysql_test_mariadb2_1 sh -c "mysql -uroot -ppassword --execute 'use vs_praktikum; stop slave; CHANGE MASTER TO MASTER_HOST = \'mysql_test_mariadb_1\', MASTER_USER = \'public\', MASTER_PASSWORD = \'mysql1pass\', MASTER_LOG_FILE = \'"+$binServer2+"\', MASTER_LOG_POS =$positionServer2; start slave;'"
